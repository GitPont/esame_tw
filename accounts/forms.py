from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django import forms

from accounts.models import UserProfile, Post


class UserCreation(UserCreationForm):
    email = forms.EmailField(required=True)
    vendor = forms.BooleanField(label='Registrarsi come venditore?', required=False)

    class Meta:
        model = User
        fields = ['vendor', 'username', 'email', 'password1', 'password2']

    def clean(self):
        super(UserCreation, self).clean()
        if User.objects.filter(email=self.cleaned_data['email']).exists():
            raise ValidationError("Email exists")
        return self.cleaned_data

    # si può fare clean_dato, all'inizio del form per condizioni extra etc... usi sempre validation error

    def save(self, commit=True):
        utente = super(UserCreation, self).save(commit=True)
        profile = UserProfile.objects.create(user=utente, is_vendor=self.cleaned_data['vendor'])
        return utente


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'body']

    def __init__(self, *args, **kwargs):
        self.seller = kwargs.pop('seller')
        self.author = kwargs.pop('author')
        self.date = kwargs.pop('date')
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        post = super(PostForm, self).save(commit=False)
        post.seller = self.seller
        post.author = self.author
        post.date = self.date
        if commit:
            post.save()
        return post

