from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


# Create your models here.
class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profile")

    is_vendor = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username


class Post(models.Model):
    title = models.TextField()
    body = models.TextField()
    seller = models.ForeignKey(User, on_delete=models.CASCADE, related_name="vendor")
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name="buyer")
    date = models.DateTimeField(blank=True)

    def __str__(self):
        return self.title
