# Generated by Django 3.0.6 on 2020-09-27 14:19

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):
    dependencies = [
        ('accounts', '0009_auto_20200923_1610'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='date',
            field=models.DateTimeField(blank=True,
                                       default=datetime.datetime(2020, 9, 27, 14, 24, 34, 817117, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
