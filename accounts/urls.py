from django.urls import path

from accounts import views
from django.contrib.auth import views as auth_views

from accounts.views import VendorView, CreatePostView, CartView, ProfileView, VProfileView, CartDisplayView

app_name = 'accounts'

urlpatterns = [
    path('registration', views.UserCreateView.as_view(), name='user-create'),
    path('login', auth_views.LoginView.as_view(), name='login'),
    path('logged_out', auth_views.LogoutView.as_view(), name='logged-out'),
    path('check_unique', views.check_username_is_unique, name='check-username-unique'),
    path('<pk>/', VendorView.as_view(), name='vendor_detail'),
    path('<pk>/post', CreatePostView.as_view(), name='add_post'),
    path('cart', CartView.as_view(), name='cart'),
    path('buy', views.buy_cart, name='buy'),
    path('user_profile', ProfileView.as_view(), name='user_profile'),
    path('vendor_profile', VProfileView.as_view(), name='vendor_profile'),
    path('cart_display/<pk>', CartDisplayView.as_view(), name='cart_display')


]
