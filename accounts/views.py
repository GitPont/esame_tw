from django.contrib.auth.models import User
from django.contrib import messages
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView, ListView
from django.views.generic.base import View
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.mixins import LoginRequiredMixin
from accounts.forms import UserCreation, PostForm
from accounts.models import Post
from cards.models import Cart, Payment, CardOrder
from django.utils import timezone


class UserCreateView(CreateView):
    form_class = UserCreation

    template_name = 'accounts/registration.html'

    success_url = reverse_lazy('homepage')


def check_username_is_unique(request):
    user = None
    username = request.GET.get('username', False)
    if username:
        user = User.objects.filter(username=username)
    return JsonResponse({'is_not_unique': True if user else False})


class VendorView(DetailView):
    model = User

    template_name = 'VendorDetail.html'

    def get_context_data(self, *, object_list=None, **kwargs):  # i dati da passare all'html
        context = super(VendorView, self).get_context_data(**kwargs)
        vendor_id = self.kwargs['pk']
        vendor = User.objects.get(id=vendor_id)
        context['Vendor'] = vendor
        context['listapost'] = Post.objects.filter(seller=vendor_id)

        return context


class CreatePostView(CreateView):  # new
    model = Post
    form_class = PostForm
    template_name = 'post.html'

    def get_form_kwargs(self):  # per passare cose al form
        kwargs = super(CreatePostView, self).get_form_kwargs()
        kwargs.update({'seller': User.objects.get(id=self.kwargs['pk'])})
        kwargs.update({'author': self.request.user})  # lo user deve essere loggato per scrivere un messaggio
        kwargs.update({'date': timezone.now()})
        return kwargs

    def get_success_url(self, **kwargs):
        vendor_id = self.kwargs['pk']
        success_url = reverse_lazy('accounts:vendor_detail', kwargs={
            'pk': vendor_id})
        return success_url


class CartView(View):
    def get(self, *args, **kwargs):
        try:
            order = Cart.objects.get(user=self.request.user, ordered=False)
            context = {
                'object': order
            }
            return render(self.request, 'cart.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("/")


def buy_cart(request):
    try:
        order = Cart.objects.get(user=request.user, ordered=False)
    except ObjectDoesNotExist:
        messages.info(request, "You do not have an active order")

    order_items = order.items.all()
    order_items.update(ordered=True)
    for item in order_items:
        item.save()

    order.ordered = True
    order.ordered_date = timezone.now()
    order.save()
    payment = Payment()
    payment.user = request.user
    payment.amount = order.get_total()
    payment.cart = order
    payment.save()

    messages.success(request, "Your order was successful!")
    return redirect("/")


class ProfileView(View):
    model = User

    template_name = 'userprofile.html'

    def get(self, *args, **kwargs):
        user_id = self.request.user.id
        user = User.objects.get(id=user_id)
        context = {
            'User': user,
            'listacquisti': Payment.objects.filter(user=user)
        }
        return render(self.request, 'userprofile.html', context)


class VProfileView(DetailView):
    model = User

    template_name = 'vendorprofile.html'

    def get(self, *args, **kwargs):
        vendor_id = self.request.user.id
        vendor = User.objects.get(id=vendor_id)
        context = {
            'Vendor': vendor,
            'listavendite': CardOrder.objects.filter(vendor=vendor_id)
        }
        return render(self.request, 'vendorprofile.html', context)


class CartDisplayView(View):
    model = Cart

    template_name = 'cartdisplay.html'

    def get(self, *args, **kwargs):
        cart_id = self.kwargs['pk']
        cart = Cart.objects.get(id=cart_id)
        context = {
            'User': self.request.user,
            'listacquisti': cart.items.all()
        }
        return render(self.request, 'cartdisplay.html', context)

