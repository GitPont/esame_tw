# Generated by Django 3.0.6 on 2020-09-27 12:46

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('cards', '0013_auto_20200927_1443'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cardorder',
            name='ordered_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 9, 27, 12, 46, 46, 705625, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='cardorder',
            name='vendor',
            field=models.ForeignKey(default=14, on_delete=django.db.models.deletion.CASCADE, related_name='venditore', to=settings.AUTH_USER_MODEL),
        ),
    ]
