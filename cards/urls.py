from django.urls import path

from django.urls import path

from .views import CreateCardView, CardDetailView, add_to_cart, remove_from_cart

app_name = 'card'

urlpatterns = [
    path('', CreateCardView.as_view(), name='add_card'),
    path('<pk>/', CardDetailView.as_view(), name='card_detail'),
    path('<pk>/add_to_cart/', add_to_cart, name='add_to_cart'),
    path('<pk>/remove_from_cart/', remove_from_cart, name='remove_from_cart')
    # se vuoi vedere esattamente un oggetto serve un
    # pk nell'url
]