from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.shortcuts import reverse

# Create your models here.

from accounts.models import UserProfile


class Card(models.Model):
    name = models.TextField()
    set = models.TextField()
    image = models.ImageField(upload_to='images/')
    price = models.FloatField()
    vendor = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class CardOrder(models.Model):
    card = models.ForeignKey(Card, on_delete=models.CASCADE, default=1)
    quantity = models.IntegerField(default=1)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    ordered = models.BooleanField(default=False)
    vendor = models.ForeignKey(User, on_delete=models.CASCADE, related_name="venditore")
    ordered_date = models.DateTimeField(default=timezone.now())

    def __str__(self):
        return f"{self.quantity} of {self.card.name} from {self.vendor.username}"

    def get_tot_price(self):
        return self.card.price * self.quantity


class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    ordered = models.BooleanField(default=False)
    items = models.ManyToManyField(CardOrder)
    start_date = models.DateTimeField(auto_now_add=True)
    ordered_date = models.DateTimeField()

    def __str__(self):
        return f"Cart of {self.user.username}"

    def get_total(self):
        total = 0
        for order_item in self.items.all():
            total += order_item.get_tot_price()
        return total


class Payment(models.Model):
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE)
    amount = models.FloatField()
    timestamp = models.DateTimeField(auto_now_add=True)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username
