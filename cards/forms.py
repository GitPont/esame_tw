from django import forms
from .models import Card


class CardForm(forms.ModelForm):
    class Meta:
        model = Card
        fields = ['name', 'set', 'image', 'price']

    def __init__(self, *args, **kwargs):
        self.vendor = kwargs.pop('vendor')
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        card = super(CardForm, self).save(commit=False)
        card.vendor = self.vendor
        if commit:
            card.save()
        return card
