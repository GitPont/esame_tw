from django.utils import timezone
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView

from django.contrib.auth.decorators import login_required

# Create your views here.
from cards.forms import CardForm
from cards.models import Card, CardOrder, Cart


class CreateCardView(CreateView):
    model = Card
    form_class = CardForm
    template_name = 'sellcard.html'

    success_url = reverse_lazy('homepage')

    def get_form_kwargs(self):  # per passare cose al form
        kwargs = super(CreateCardView, self).get_form_kwargs()
        kwargs.update({'vendor': self.request.user})
        return kwargs


class CardDetailView(DetailView):
    model = Card
    form_class = CardForm
    template_name = 'CardDetail.html'

    def get_context_data(self, *, object_list=None, **kwargs):  # i dati da passare all'html
        context = super(CardDetailView, self).get_context_data(**kwargs)
        card_id = self.kwargs['pk']
        card = Card.objects.get(id=card_id)
        context['Card'] = card
        card_orders = CardOrder.objects.filter(card=card, ordered=True)
        carts = []
        raccomendations = []
        for ca in Cart.objects.filter(ordered=True):
            for co in card_orders:
                if co in ca.items.all():
                    carts.append(ca)
        for ca in carts:
            for co in ca.items.all():
                if co.card.name != card.name:
                    if co.card not in raccomendations:
                        raccomendations.append(co.card)

        context['raccomendations'] = raccomendations

        return context


@login_required  # non so quanto sia utile
def add_to_cart(request, pk):
    card_id = pk
    card = Card.objects.get(id=card_id)
    card_order, created = CardOrder.objects.get_or_create(card=card, user=request.user, vendor=card.vendor,
                                                          ordered=False)
    cart_qs = Cart.objects.filter(user=request.user, ordered=False)
    if cart_qs.exists():
        cart = cart_qs[0]
        if cart.items.filter(id=card_order.id).exists():  # da controllare
            card_order.quantity += 1
            card_order.save()
            messages.info(request, "This item quantity was updated")
        else:
            messages.info(request, "This item was added to the cart")
            cart.items.add(card_order)
    else:
        ordered_date = timezone.now()
        cart = Cart.objects.create(user=request.user, ordered_date=ordered_date)
        card_order.ordered_date = ordered_date
        card_order.save()
        cart.items.add(card_order)
        messages.info(request, "This item was added to the cart")
    return HttpResponseRedirect(reverse_lazy('card:card_detail', kwargs={
        'pk': card_id}))


@login_required
def remove_from_cart(request, pk):
    card_id = pk
    card = Card.objects.get(id=card_id)
    card_order = CardOrder.objects.filter(card=card, user=request.user, vendor=card.vendor, ordered=False)[0]
    cart_qs = Cart.objects.filter(user=request.user, ordered=False)
    if cart_qs.exists():
        cart = cart_qs[0]
        if cart.items.filter(id=card_order.id).exists():  # da controllare
            cart.items.remove(card_order)
            messages.info(request, "This item was removed from the cart")
        else:
            # il carrello non contiene l'oggetto
            messages.info(request, "This item was not in the cart")
            return HttpResponseRedirect(
                reverse_lazy('card:card_detail', kwargs={  # per sicurezza metti il return in tutte le condizioni
                    'pk': card_id}))
    else:
        # lo user non ha un carrello
        messages.info(request, "You don't have a cart")
        return HttpResponseRedirect(reverse_lazy('card:card_detail', kwargs={
            'pk': card_id}))
    return HttpResponseRedirect(reverse_lazy('card:card_detail', kwargs={
        'pk': card_id}))

