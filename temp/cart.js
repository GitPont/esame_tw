
$(document).ready(function () {
    $(function() {
        table=$("#OrderTableTable").DataTable({ bFilter: true, bInfo: false , paging: true,lengthChange: false

    })
        for (column in table.columns)
        {
            var select = $('<select />')
        .appendTo(
            table.column(column).footer()
        )
        .on( 'change', function () {
            table
                .column( column )
                .search( $(this).val() )
                .draw();
        } );
        }


    // Get the search data for the first column and add to the select list
    table
        .columns
        .cache( 'search' )
        .sort()
        .unique()
        .each( function ( d ) {
            select.append( $('<option value="'+d+'">'+d+'</option>') );
        } );

});
    });
