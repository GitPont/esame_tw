# Generated by Django 3.0.6 on 2020-09-25 16:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cards', '0010_auto_20200925_1751'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cardorder',
            name='card',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='cards.Card'),
        ),
    ]
