from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import View
from cards.models import Card

# class HomePageView(TemplateView):
#   template_name = 'home.html'

'''
class HomePageView(ListView):
    model = Card
    template_name = 'home.html'
'''


class HomePageView(View):
    def get(self, *args, **kwargs):
        context = {
            'cards': Card.objects.all(),
            'prime': Card.objects.all()[:10]
        }
        return render(self.request, 'home.html', context)


